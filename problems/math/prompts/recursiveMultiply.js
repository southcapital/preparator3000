/**
  * Write a recursive function to multiply two positive integers without using the * operator.
  * You can use addition, substraction, and bit shifting operators but you should minimize the number
  * of those operations.
  *
  * recursiveMultiply(2, 4) // 8
  * recursiveMultiply(3, 10) // 30
  *
  * Extra Credit: Handle multiplication of negative numbers
  * recursiveMultiply(2, 4) // 8
  */

  const recursiveMultiply = (num1, num2) => {
    let accum = 0;
    const dive = (count, accum) => {
      if(count === 0) {
        return accum;
      } else {
        count--;
        accum += num2;
        return dive(count, accum);
      }
    }
    return dive(num1, accum);
  };
module.exports = { recursiveMultiply };
