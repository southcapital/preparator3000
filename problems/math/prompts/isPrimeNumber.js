/**
  * A prime number is a number that is only divisible by 1 and itself.
  *
  * Implement isPrimeNumber to check an input number and return a
  * boolean indicating if it is prime or not.
  */

  const isPrimeNumber = number => {
    let i = number - 1;
    while(i > 1) {
      if(number % i === 0) {
        return false;
      }
      i--;
    }
    return true;
  };
module.exports = { isPrimeNumber };
