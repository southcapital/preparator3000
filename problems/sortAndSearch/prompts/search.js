/**
  * Implement two functions to find the index of the element k in a sorted array of numbers.
  * Both algorithms should take log(n) time.
  *
  * Unsuccessful searches should return -1.
  *
  * For Example:
  * searchIterative([1,2,3,4,5,6], 3); // => 2
  * searchIterative([1,2,3,4,5,6], 9); // => -1
  */


const searchIterative = (nums, k) => {
  console.log(nums);

  let center = Math.floor(nums.length / 2);
  console.log(nums[center]);
  if (nums[center] === k) {
    return center;
  }
  while (nums[center] !== k) {
    console.log(nums);
    console.log(center);
    center = Math.floor(nums.length / 2);
    if (k < nums[center]) {
      nums = nums.splice(0, center);
    } else {
      nums = nums.splice(center);
    }
  }
  return center;
}

const searchRecursive = (nums, k, i) => {
  // TODO: Implement recursive search
}

module.exports = { searchIterative, searchRecursive };
