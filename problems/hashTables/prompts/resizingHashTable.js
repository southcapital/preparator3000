/**
 * Create a hash table with `insert()`, `retrieve()`, and `remove()` methods.
 * Be sure to handle hashing collisions correctly.
 * Set your hash table up to double the storage limit as
 * soon as the total number of items stored is greater than
 * 3/4th of the number of slots in the storage array.
 * Resize by half whenever utilization drops below 1/4.
 */

const makeHashTable = () => {
  const result = {};
  let storage = [];

  let storageLimit = 4;
  let size = 0;

  result.insert = (key, value) => {
    let index = getIndexBelowMaxForKey(key, storageLimit);
    storage[index] = storage[index] || [];
    let bucket = storage[index];
    if (!bucket) {
      bucket = [];
      storage[index] = value;
    }
    let inserted = false;
    bucket.forEach(tuple => {
      if (tuple[0] === key) {
        tuple[1] = value;
        inserted = true;
      }
    });
    if (!inserted) {
      bucket.push([key, value]);
      size++;
      if (size >= storageLimit * 0.75) {
        result.resize(storageLimit * 2);
      }
    }
  };

  result.retrieve = (key) => {
    let index = getIndexBelowMaxForKey(key, storageLimit);
    let bucket = storage[index];
    if (!bucket) {
      return undefined;
    }
    for (let i = 0; i < bucket.length; i++) {
      let tuple = bucket[i];
      if (tuple[0] === key) {
        return tuple[1];
      }
    }
  };

  result.remove = (key) => {
    let index = getIndexBelowMaxForKey(key, storageLimit);
    let bucket = storage[index];
    if (!bucket) {
      return undefined;
    }
    for (let i = 0; i < bucket.length; i++) {
      let tuple = bucket[i];
      if (tuple[0] === key) {
        let spliced = tuple;
        bucket.splice(i, 1);
        size--;
        if (size <= storageLimit * 0.25) {
          result.resize(storageLimit / 2);
        }
        return spliced;
      }
    }
  };

  result.resize = (newSize) => {
    let holder = storage;
    storageLimit = newSize;
    storage = [];
    size = 0;
    holder.forEach(bucket => {
      if (!bucket) {
        return;
      }
      bucket.forEach(tuple => {
        result.insert(tuple[0], tuple[1]);
      });
    });
  }

  return result;
};

// This is a "hashing function". You don't need to worry about it, just use it
// to turn any string into an integer that is well-distributed between
// 0 and max - 1
const getIndexBelowMaxForKey = (str, max) => {
  let hash = 0;
  for (let i = 0; i < str.length; i++) {
    hash = (hash<<5) + hash + str.charCodeAt(i);
    hash = hash & hash; // Convert to 32bit integer
    hash = Math.abs(hash);
  }
  return hash % max;
};

module.exports = { makeHashTable };
