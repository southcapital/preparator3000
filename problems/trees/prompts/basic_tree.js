'use strict';

/**
  * Implement a basic tree with the methods addChild and contains.
  * Every tree's children should be accessible through .children[]
  * Children should be able to add children directly.
  */

  class Tree {
    constructor(val) {
      if(val){
        this.value = val;
      }
    }
    addChild(val) {
      let kid = new Tree(val);
      if(!this.children) this.children = [];
      this.children.push(kid);
      return kid;
    }
    contains(val) {
      if(this.value === val) {
        return true;
      } else if(this.children) {
        for(let i = 0; i < this.children.length; i++) {
          return this.children[i].contains(val);
        }
      }
      return false;
    }
  };

module.exports = { Tree };
