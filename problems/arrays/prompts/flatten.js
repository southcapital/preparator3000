'use strict';

// flatten a nested array into a flat (1D) array
const flatten = (arrays, flattened) => {
  flattened = flattened || [];
  arrays.forEach(el => {
    if (Array.isArray(el)) {
      return flatten(el, flattened);
    }
    flattened.push(el);
  });
  return flattened;
};

module.exports = { flatten };
