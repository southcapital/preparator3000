'use strict';

// returns an array without duplicates
const removeDuplicates = (array) => {
  return array.reduce((noDups, current) => {
    if (!noDups.includes(current)) {
      noDups.push(current);
    }
    return noDups;
  }, []);
};

module.exports = { removeDuplicates };
