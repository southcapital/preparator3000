'use strict';

// determine if some elements in the array pass a truth test
const some = (array, truthTest) => {
  return array.reduce((bool, current) => {
    if (truthTest(current)) {
      bool = true;
    }
    return bool;
  }, false)
};

module.exports = { some };
