'use strict';

// reverse and return an array in place (space complexity of O(1))
const reverseArrayInPlace = array => {
  for (let i = 0; i < array.length / 2; i++) {
    let holder = array[i];
    array[i] = array[array.length - 1 - i];
    array[array.length - 1 - i] = holder;
  }
  return array;
};

module.exports = { reverseArrayInPlace };
