'use strict';

/* return the position of a number [row, col] from a sorted matrix
 *
 * example:
 * var matrix = [
 *   [2, 4, 6, 7],
 *   [5, 7, 8, 11],
 *   [9, 10, 15, 18],
 *   [11, 16, 20, 25],
 * ]
 *
 * searchSortedMatrix(matrix, 15); // returns [2, 2];
 * searchSortedMatrix(matrix, 15); // returns [2, 2];
 */

const searchSortedMatrix = (matrix, number) => {
  let row = 0, col = 0;
  for (let i = 0; i < matrix.length; i++) {
    if (matrix[i].includes(number)) {
      row = i;
      break;
    }
  }
  if (!row) {
    return null;
  }
  for (let i = 0; i < matrix[row].length; i++) {
    if (matrix[row][i] === number) {
      col = i;
      break;
    }
  }
  return [row, col];
};

module.exports = { searchSortedMatrix };
