'use strict';

// determine if every element in the array passes a truth test
const every = (array, truthTest) => {
  let bool = true;
  for (let i = 0; i < array.length; i++) {
    if (!truthTest(array[i])) {
      bool = false;
      break;
    }
  }
  return bool;
};

module.exports = { every };
